﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            Task3 a = new Task3();                       
            Console.WriteLine(a.RemoveDigitFromNumber(123, 5));
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source, out int res))
                return res;
            else
                throw new ArgumentException("Input data error");
        }

        public int Multiplication(int num1, int num2)
        {
            int res = 0;
            for (int i=1; i<=Math.Abs(num2); ++i)
            {
                res += Math.Abs(num1);
            }
            if (((num1 > 0) && (num2 > 0)) || ((num1 < 0) && (num2 < 0)))
                return res;
            else
                return -res;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            do
            {
                if (int.TryParse(input, out result) && (result >= 0))
                    return true;
                else
                {
                    Console.WriteLine("Input data error. Try again");
                    input = Console.ReadLine();
                }
            }
            while (true);
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> res=new List<int>();
            
            for(int i=0; i<naturalNumber; ++i)
            {
                res.Add(i*2);
            }
            return res;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            do
            {
                if (int.TryParse(input, out result) && (result >= 0))
                    return true;
                else
                {
                    Console.WriteLine("Input data error. Try again");
                    input = Console.ReadLine();
                }
            }
            while (true);
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string res = source.ToString();
            int indexOfDigit = res.IndexOf(digitToRemove.ToString());
            while (indexOfDigit != -1)
            {
                res = res.Remove(indexOfDigit, 1);
                indexOfDigit = res.IndexOf(digitToRemove.ToString());
            }
            return res;
        }
    }
}
